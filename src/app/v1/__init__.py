from fastapi import APIRouter

from .coupon import router as coupon_router
from .user import router as user_router
from .voucher import router as voucher_router

v1_router = APIRouter()
v1_router.include_router(user_router)
v1_router.include_router(coupon_router)
v1_router.include_router(voucher_router)


__all__ = ["v1_router"]
