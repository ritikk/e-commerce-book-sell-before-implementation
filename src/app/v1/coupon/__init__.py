from fastapi import APIRouter

from .controller.coupon_controller import CouponController
from .controller.coupon_controller import router as coupon_router
from .repository import CouponRepo
from .repository.coupon_repo import CouponRepo

router = APIRouter()
router.include_router(coupon_router, prefix="/coupon", tags=["Coupon"])

__all__ = ["router"]
