from typing import List

from fastapi_utils.cbv import cbv
from fastapi_utils.inferring_router import InferringRouter
from pydantic.schema import UUID

from ..schema import (CreateCouponRequest, CreateCouponResponse,
                      GetAllCouponsResponse)
from ..service.coupon_command import CouponCommandService
from ..service.coupon_query import CouponQueryService

router = InferringRouter()


@cbv(router)
class CouponController:

    @router.post(
        "/create_coupon",
        response_model=CreateCouponResponse,
        # responses={"400": {"model": ExceptionResponseSchema}},
        summary="Create Coupon",
    )
    async def create_coupon(self, request: CreateCouponRequest):
        return await CouponCommandService().create_coupon(**request.dict())

    @router.get(
        "/get_coupon/{id}",
        response_model=CreateCouponResponse,
        # responses={"400": {"model": ExceptionResponseSchema}},
        summary="Get Coupon By ID",
    )
    async def get_coupon(self, id: UUID):
        return await CouponQueryService().get_coupon(id)

    @router.get(
        "/get_all_coupons",
        response_model=List[GetAllCouponsResponse],
        # responses={"400": {"model": ExceptionResponseSchema}},
        summary="Get All Coupons",
    )
    async def get_all_coupons(self):
        return await CouponQueryService().get_all_coupons()

    @router.put(
        "/update_coupon/{id}",
        # response_model=CreateCouponResponse,
        # responses={"400": {"model": ExceptionResponseSchema}},
        summary="Update Coupon By ID",
    )
    async def update_coupon(self, id:UUID, request: CreateCouponRequest):
        return await CouponCommandService().update_coupon(id, **request.dict())

    @router.delete(
        "/delete_coupon/{id}",
        # responses={"400": {"model": ExceptionResponseSchema}},
        summary="Delete Coupon By ID",
    )
    async def delete_coupon(self, id: UUID):
        return await CouponCommandService().delete_coupon(id)
