import uuid
from datetime import date, datetime

from sqlalchemy import (BigInteger, Boolean, Column, Date, Float, ForeignKey,
                        Integer, String)
from sqlalchemy.dialects.postgresql import UUID

from core.db import Base
from core.db.mixins import TimestampMixin


class CouponModel(Base, TimestampMixin):
    __tablename__ = "coupons"

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    category = Column(String, nullable=False)
    coupon_code = Column(String, nullable=False)
    amount = Column(Float, nullable=True)
    product_name = Column(String, nullable=True)
    valid_from = Column(Date, nullable=False)
    valid_to = Column(Date, nullable=False)
    is_active = Column(Boolean, nullable=False, default=True)
    use_limit = Column(BigInteger, nullable=False)
    disc_percent = Column(Float, nullable=True)
    coup_desc = Column(String, nullable=False)

    @classmethod
    def create(cls,
               category: str,
               coupon_code: str,
               amount: float,
               product_name: str,
               valid_from: date,
               valid_to: date,
               is_active: bool,
               use_limit: int,
               disc_percent: float,
               coup_desc: str):
        id = uuid.uuid4()
        return cls(id=id,
                   category=category,
                   coupon_code=coupon_code,
                   amount=amount,
                   product_name=product_name,
                   valid_from=valid_from,
                   valid_to=valid_to,
                   is_active=is_active,
                   use_limit=use_limit,
                   disc_percent=disc_percent,
                   coup_desc=coup_desc)

