from abc import ABCMeta, abstractmethod
from typing import List

from pydantic.schema import UUID
from sqlalchemy import or_, select

from core.db import session

from ..models import CouponModel


class CouponRepo:
    async def get_by_id(self, id: UUID):
        return await session.get(CouponModel, id)

    async def get_coupons(self):
        query = await session.execute(select(CouponModel))
        return query.scalars().all()

    async def save(self, coupon: CouponModel):
        session.add(coupon)
        return coupon

    async def delete(self, coupon: CouponModel):
        await session.delete(coupon)
        return None

    async def update(self, coupon: CouponModel):
        return coupon
