from datetime import date, datetime
from typing import Optional

from fastapi_utils.api_model import APIModel
from pydantic import Field
from pydantic.schema import UUID


class Coupon(APIModel):
    id: UUID = Field(None, description="ID")
    category: str = Field(None, description="Category")
    coupon_code: str = Field(None, description="Coupon Code")
    amount: Optional[float] = Field(None, description="Amount")
    product_name: Optional[str] = Field(None, description="Product Name")
    valid_from: date = Field(None, description="Valid From")
    valid_to: date = Field(None, description="Valid To")
    is_active: bool = Field(None, description="Is Active")
    use_limit: int = Field(None, description="Use Limit")
    disc_percent: Optional[float] = Field(None, description="Discount Percentage")
    coup_desc: str = Field(None, description="Coupon Description")
