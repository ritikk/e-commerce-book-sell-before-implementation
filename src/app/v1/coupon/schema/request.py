from datetime import date
from typing import Optional

from pydantic import BaseModel


class CreateCouponRequest(BaseModel):
    category: str
    coupon_code: str
    amount: Optional[float]
    product_name: Optional[str]
    valid_from: date
    valid_to: date
    is_active: bool
    use_limit: int
    disc_percent: Optional[float]
    coup_desc: str

    class Config:
        schema_extra = {
            "example": {
                "category": "Cart Category",
                "coupon_code": "NEWUSER20",
                "amount": 5000.0,
                "product_name": "RTX 3080 ROG Strix",
                "valid_from": "2022-07-28",
                "valid_to": "2022-08-27",
                "is_active": True,
                "use_limit": 1,
                "disc_percent": 20.0,
                "coup_desc": "Discount for newuser"
            }
        }


