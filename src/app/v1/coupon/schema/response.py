from datetime import date, datetime
from typing import Optional

from pydantic import BaseModel
from pydantic.schema import UUID


class CreateCouponResponse(BaseModel):
    id: UUID
    category: str
    couponCode: str
    amount: Optional[float]
    productName: Optional[str]
    validFrom: date
    validTo: date
    isActive: bool
    useLimit: int
    discPercent: Optional[float]
    coupDesc: str

    class Config:
        orm_mode = True
        schema_extra = {
            "example": {
                "category": "Cart Category",
                "coupon_code": "NEWUSER20",
                "amount": 5000.0,
                "product_name": "RTX 3080 ROG Strix",
                "valid_from": "2022-07-28",
                "valid_to": "2022-08-27",
                "is_active": True,
                "use_limit": 1,
                "disc_percent": 20.0,
                "coup_desc": "Discount for new user"
            }
        }


class GetAllCouponsResponse(BaseModel):
    category: str
    couponCode: str
    amount: Optional[float]
    productName: Optional[str]
    validFrom: date
    validTo: date
    isActive: bool
    useLimit: int
    discPercent: Optional[float]
    coupDesc: str

    class Config:
        orm_mode = True
