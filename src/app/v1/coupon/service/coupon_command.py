from datetime import date

from distlib import database
from fastapi import HTTPException
from pydantic.schema import UUID
from pythondi import inject
from starlette.responses import JSONResponse

from core.db import Propagation, Transactional

from ..models import CouponModel
from ..repository import CouponRepo
from ..schema import Coupon


class CouponCommandService:
    @inject()
    def __init__(self, coupon_repo: CouponRepo):
        self.coupon_repo = coupon_repo

    @Transactional(propagation=Propagation.REQUIRED)
    async def create_coupon(self,
                            category: str,
                            coupon_code: str,
                            amount: float,
                            product_name: str,
                            valid_from: date,
                            valid_to: date,
                            is_active: bool,
                            use_limit: int,
                            disc_percent: float,
                            coup_desc: str):
        coupon = CouponModel.create(category=category,
                                    coupon_code=coupon_code,
                                    amount=amount,
                                    product_name=product_name,
                                    valid_from=valid_from,
                                    valid_to=valid_to,
                                    is_active=is_active,
                                    use_limit=use_limit,
                                    disc_percent=disc_percent,
                                    coup_desc=coup_desc)
        coupon = await self.coupon_repo.save(coupon=coupon)
        return Coupon.from_orm(coupon)

    @Transactional(propagation=Propagation.REQUIRED)
    async def update_coupon(self,
                            id: UUID,
                            category: str,
                            coupon_code: str,
                            amount: float,
                            product_name: str,
                            valid_from: date,
                            valid_to: date,
                            is_active: bool,
                            use_limit: int,
                            disc_percent: float,
                            coup_desc: str):
        coupon = await self.coupon_repo.get_by_id(id=id)
        if not coupon:
            raise HTTPException(status_code=404, detail="Coupon Not Found")

        if category:
            coupon.category = category

        if coupon_code:
            coupon.coupon_code = coupon_code

        if amount:
            coupon.amount = amount

        if product_name:
            coupon.product_name = product_name

        if valid_from:
            coupon.valid_from = valid_from

        if valid_to:
            coupon.valid_to = valid_to

        if is_active:
            coupon.is_active = is_active

        if use_limit:
            coupon.use_limit = use_limit

        if disc_percent:
            coupon.disc_percent = disc_percent

        if coupon.coup_desc:
            coupon.coup_desc = coup_desc

        coupon = await self.coupon_repo.update(coupon)
        return JSONResponse(content="Data Updated Successfully", status_code=200)

    @Transactional(propagation=Propagation.REQUIRED)
    async def delete_coupon(self, id: UUID):
        coupon = await self.coupon_repo.get_by_id(id=id)
        await self.coupon_repo.delete(coupon=coupon)
        return JSONResponse(content="Data Deleted Successfully", status_code=200)
