from typing import Optional

from pydantic.schema import UUID
from pythondi import inject
from starlette.responses import JSONResponse

from core.db import Propagation, Transactional

from ..repository import CouponRepo
from ..schema import Coupon


class CouponQueryService:
    @inject()
    def __init__(self, coupon_repo: CouponRepo):
        self.coupon_repo = coupon_repo

    async def get_coupon(self, id: UUID):
        coupon = await self.coupon_repo.get_by_id(id=id)
        return Coupon.from_orm(coupon)

    async def get_all_coupons(self):
        coupon = await self.coupon_repo.get_coupons()
        result = []
        for i in coupon:
            result.append(Coupon.from_orm(i))
        return result
