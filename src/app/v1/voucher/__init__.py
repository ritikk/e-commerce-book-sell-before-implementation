from fastapi import APIRouter

from .controller.voucher_controller import VoucherController
from .controller.voucher_controller import router as voucher_router
from .repository import VoucherRepo
from .repository.voucher_repo import VoucherRepo

router = APIRouter()
router.include_router(voucher_router, prefix="/voucher", tags=["Voucher"])

__all__ = ["router"]
