from typing import List, NoReturn, Union

from fastapi_utils.cbv import cbv
from fastapi_utils.inferring_router import InferringRouter
from pydantic.schema import UUID

from ..schema import (CreateVoucherRequest, CreateVoucherResponse,
                      GetAllVouchersResponse, Voucher)
from ..service.voucher_command import VoucherCommandService
from ..service.voucher_query import VoucherQueryService

router = InferringRouter()


@cbv(router)
class VoucherController:

    @router.post(
        "/create_voucher",
        response_model=CreateVoucherResponse,
        # responses={"400": {"model": ExceptionResponseSchema}},
        summary="Create Voucher",
    )
    async def create_voucher(self, request: CreateVoucherRequest):
        return await VoucherCommandService().create_voucher(**request.dict())

    @router.get(
        "/get_voucher/{id}",
        response_model=CreateVoucherResponse,
        # responses={"400": {"model": ExceptionResponseSchema}},
        summary="Get Voucher By ID",
    )
    async def get_voucher(self, id: UUID):
        return await VoucherQueryService().get_voucher(id)

    @router.get(
        "/get_all_vouchers",
        response_model=List[GetAllVouchersResponse],
        # responses={"400": {"model": ExceptionResponseSchema}},
        summary="Get All Vouchers",
    )
    async def get_all_Vouchers(self):
        return await VoucherQueryService().get_all_vouchers()

    @router.put(
        "/update_voucher/{id}",
        # response_model=CreateVoucherResponse,
        # responses={"400": {"model": ExceptionResponseSchema}},
        summary="Update Voucher By ID",
    )
    async def update_voucher(self, id: UUID, request: CreateVoucherRequest):
        return await VoucherCommandService().update_voucher(id, **request.dict())

    @router.delete(
        "/delete_voucher/{id}",
        # responses={"400": {"model": ExceptionResponseSchema}},
        summary="Delete Voucher By ID",
    )
    async def delete_voucher(self, id: UUID):
        return await VoucherCommandService().delete_voucher(id)
