import uuid
from datetime import date, datetime

from sqlalchemy import (BigInteger, Boolean, Column, Date, Float, ForeignKey,
                        Integer, String)
from sqlalchemy.dialects.postgresql import UUID

from core.db import Base
from core.db.mixins import TimestampMixin


class VoucherModel(Base, TimestampMixin):
    __tablename__ = "vouchers"

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    category = Column(String, nullable=False)
    voucher_code = Column(String, nullable=False)
    amount = Column(Float, nullable=True)
    validity_limited = Column(Boolean, nullable=False)
    valid_from = Column(Date, nullable=True)
    valid_to = Column(Date, nullable=True)
    disc_percent = Column(Float, nullable=True)
    is_active = Column(Boolean, nullable=False, default=True)
    use_limit = Column(BigInteger, nullable=False)
    vouch_desc = Column(String, nullable=False)

    @classmethod
    def create(cls,
               category: str,
               voucher_code: str,
               amount: float,
               validity_limited: bool,
               valid_from: date,
               valid_to: date,
               disc_percent: float,
               is_active: bool,
               use_limit: int,
               vouch_desc: str):
        id = uuid.uuid4()
        return cls(id=id,
                   category=category,
                   voucher_code=voucher_code,
                   amount=amount,
                   validity_limited=validity_limited,
                   valid_from=valid_from,
                   valid_to=valid_to,
                   disc_percent=disc_percent,
                   is_active=is_active,
                   use_limit=use_limit,
                   vouch_desc=vouch_desc)
