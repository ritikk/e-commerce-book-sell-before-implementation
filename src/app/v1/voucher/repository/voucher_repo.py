from abc import ABCMeta, abstractmethod
from typing import List

from pydantic.schema import UUID
from sqlalchemy import or_, select

from core.db import session

from ..models import VoucherModel


class VoucherRepo:
    async def get_by_id(self, id: UUID):
        return await session.get(VoucherModel, id)

    async def get_vouchers(self):
        query = await session.execute(select(VoucherModel))
        return query.scalars().all()

    async def save(self, voucher: VoucherModel):
        session.add(voucher)
        return voucher

    async def delete(self, voucher: VoucherModel):
        await session.delete(voucher)
        return None

    async def update(self, voucher: VoucherModel):
        return voucher
