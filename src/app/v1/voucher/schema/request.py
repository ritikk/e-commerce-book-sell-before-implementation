from datetime import date
from typing import Optional

from pydantic import BaseModel


class CreateVoucherRequest(BaseModel):
    category: str
    voucher_code: str
    amount: Optional[float]
    validity_limited: bool
    valid_from: Optional[date]
    valid_to: Optional[date]
    disc_percent: Optional[float]
    is_active: bool
    use_limit: int
    vouch_desc: str

    class Config:
        schema_extra = {
            "example": {
                "category": "Cash Category",
                "voucher_code": "GETCASH2000",
                "amount": 2000.0,
                "validity_limited": True,
                "valid_from": "2022-07-28",
                "valid_to": "2022-08-27",
                "disc_percent": 20.0,
                "is_active": True,
                "use_limit": 1,
                "vouch_desc": "Cash for User"
            }
        }
