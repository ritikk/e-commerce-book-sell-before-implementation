from datetime import date, datetime
from typing import Optional

from pydantic import BaseModel
from pydantic.schema import UUID


class CreateVoucherResponse(BaseModel):
    id: UUID
    category: str
    voucherCode: str
    amount: Optional[float]
    validityLimited: bool
    validFrom: Optional[date]
    validTo: Optional[date]
    discPercent: Optional[float]
    isActive: bool
    useLimit: int
    vouchDesc: str

    class Config:
        orm_mode = True
        schema_extra = {
            "example": {
                "category": "Cash Category",
                "voucher_code": "GETCASH2000",
                "amount": 2000.0,
                "validity_limited": True,
                "valid_from": "2022-07-28",
                "valid_to": "2022-08-27",
                "disc_percent": 20.0,
                "is_active": True,
                "use_limit": 1,
                "vouch_desc": "Cash for User"
            }
        }


class GetAllVouchersResponse(BaseModel):
    category: str
    voucherCode: str
    amount: Optional[float]
    validityLimited: bool
    validFrom: date
    validTo: date
    discPercent: Optional[float]
    isActive: bool
    useLimit: int
    vouchDesc: str

    class Config:
        orm_mode = True
