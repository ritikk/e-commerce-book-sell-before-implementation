from datetime import date, datetime
from typing import Optional

from fastapi_utils.api_model import APIModel
from pydantic import Field
from pydantic.schema import UUID


class Voucher(APIModel):
    id: UUID = Field(None, description="ID")
    category: str = Field(None, description="Category")
    voucher_code: str = Field(None, description="Voucher Code")
    amount: Optional[float] = Field(None, description="Amount")
    validity_limited: bool = Field(None, description="Validity Limited")
    valid_from: Optional[date] = Field(None, description="Valid From")
    valid_to: Optional[date] = Field(None, description="Valid To")
    disc_percent: Optional[float] = Field(None, description="Discount Percentage")
    is_active: bool = Field(None, description="Is Active")
    use_limit: int = Field(None, description="Use Limit")
    vouch_desc: str = Field(None, description="Voucher Description")
