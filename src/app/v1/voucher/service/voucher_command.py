from datetime import date

from distlib import database
from fastapi import HTTPException
from pydantic.schema import UUID
from pythondi import inject
from starlette.responses import JSONResponse

from core.db import Propagation, Transactional

from ..models import VoucherModel
from ..repository import VoucherRepo
from ..schema import Voucher


class VoucherCommandService:
    @inject()
    def __init__(self, voucher_repo: VoucherRepo):
        self.voucher_repo = voucher_repo

    @Transactional(propagation=Propagation.REQUIRED)
    async def create_voucher(self,
                             category: str,
                             voucher_code: str,
                             amount: float,
                             validity_limited: bool,
                             valid_from: date,
                             valid_to: date,
                             disc_percent: float,
                             is_active: bool,
                             use_limit: int,
                             vouch_desc: str):
        voucher = VoucherModel.create(category=category,
                                      voucher_code=voucher_code,
                                      amount=amount,
                                      validity_limited=validity_limited,
                                      valid_from=valid_from,
                                      valid_to=valid_to,
                                      disc_percent=disc_percent,
                                      is_active=is_active,
                                      use_limit=use_limit,
                                      vouch_desc=vouch_desc)
        voucher = await self.voucher_repo.save(voucher=voucher)
        return Voucher.from_orm(voucher)

    @Transactional(propagation=Propagation.REQUIRED)
    async def update_voucher(self,
                             id: UUID,
                             category: str,
                             voucher_code: str,
                             amount: float,
                             validity_limited: bool,
                             valid_from: date,
                             valid_to: date,
                             disc_percent: float,
                             is_active: bool,
                             use_limit: int,
                             vouch_desc: str):
        voucher = await self.voucher_repo.get_by_id(id=id)
        if not voucher:
            raise HTTPException(status_code=404, detail="Voucher Not Found")

        if category:
            voucher.category = category

        if voucher_code:
            voucher.voucher_code = voucher_code

        if amount:
            voucher.amount = amount

        if validity_limited:
            voucher.validity_limited = validity_limited

        if valid_from:
            voucher.valid_from = valid_from

        if valid_to:
            voucher.valid_to = valid_to

        if is_active:
            voucher.is_active = is_active

        if use_limit:
            voucher.use_limit = use_limit

        if disc_percent:
            voucher.disc_percent = disc_percent

        if voucher.vouch_desc:
            voucher.vouch_desc = vouch_desc

        voucher = await self.voucher_repo.update(voucher)
        return JSONResponse(content="Data Updated Successfully", status_code=200)

    @Transactional(propagation=Propagation.REQUIRED)
    async def delete_voucher(self, id: UUID):
        voucher = await self.voucher_repo.get_by_id(id=id)
        await self.voucher_repo.delete(voucher=voucher)
        return JSONResponse(content="Data Deleted Successfully", status_code=200)
