from typing import Optional

from pydantic.schema import UUID
from pythondi import inject
from starlette.responses import JSONResponse

from core.db import Propagation, Transactional

from ..repository import VoucherRepo
from ..schema import Voucher


class VoucherQueryService:
    @inject()
    def __init__(self, voucher_repo: VoucherRepo):
        self.voucher_repo = voucher_repo

    async def get_voucher(self, id: UUID):
        voucher = await self.voucher_repo.get_by_id(id=id)
        return Voucher.from_orm(voucher)

    async def get_all_vouchers(self):
        voucher = await self.voucher_repo.get_vouchers()
        result = []
        for i in voucher:
            result.append(Voucher.from_orm(i))
        return result
